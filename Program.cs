using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Введіть ідентифікатори, розділені пробілами:");
        string input = Console.ReadLine();

        string[] identifiers = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);

        Console.WriteLine("Результати перевірки:");

        foreach (string identifier in identifiers)
        {
            if (IsValidIdentifier(identifier))
            {
                Console.WriteLine($"{identifier}: Правильний ідентифікатор");
            }
            else
            {
                Console.WriteLine($"{identifier}: Неправильний ідентифікатор");
            }
        }
    }

    static bool IsValidIdentifier(string identifier)
    {
        // Перевірка чи ідентифікатор не порожній
        if (string.IsNullOrEmpty(identifier))
            return false;

        // Перевірка першого символу на літеру або підкреслення
        if (!char.IsLetter(identifier[0]) && identifier[0] != '_')
            return false;

        // Перевірка решти символів
        for (int i = 1; i < identifier.Length; i++)
        {
            char ch = identifier[i];
            if (!char.IsLetterOrDigit(ch) && ch != '_')
                return false;
        }

        // Перевірка на використання зарезервованих слів
        if (IsReservedWord(identifier))
            return false;

        return true;
    }

    static bool IsReservedWord(string identifier)
    {
        // Перевірка на використання зарезервованих слів мови C#
        string[] reservedWords = { "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char", "checked",
                                   "class", "const", "continue", "decimal", "default", "delegate", "do", "double", "else",
                                   "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "foreach",
                                   "goto", "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace",
                                   "new", "null", "object", "operator", "out", "override", "params", "private", "protected",
                                   "public", "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc",
                                   "static", "string", "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong",
                                   "unchecked", "unsafe", "ushort", "using", "var", "virtual", "void", "volatile", "while" };
        return Array.Exists(reservedWords, word => word == identifier);
    }
}
